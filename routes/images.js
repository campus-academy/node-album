const express = require('express');
const router = express.Router();
const imageController = require('../controller/imageController');

router.get('/', imageController.index)
router.post('/store', imageController.store)
router.get('/create', imageController.create)
router.post('/delete-one/:id', imageController.deleteOne)
router.get('/show/:id', imageController.show)
router.get('/edit/:id', imageController.edit)
router.post('/update/:id', imageController.update)

module.exports = router;