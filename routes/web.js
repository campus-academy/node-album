const express = require('express');
const router = express.Router();
const webController = require('../controller/webController');

router.get('/', webController.home)

module.exports = router;