const express = require('express');
const path = require('path');
const app = express();
const http = require('http');
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


const webRoutes = require('./routes/web')
const imagesRoutes = require('./routes/images');
app.use('/', webRoutes);
app.use('/images', imagesRoutes);

// app.use('*', function(req, res) {
//     console.log('error')
// })

app.set('port', process.env.PORT || 3000);
http.createServer(app)
    .listen(
        app.get('port'),
        () => {
            console.log(`Express.js server ecoutes sur le port ${app.get('port')}`);
});
  
