const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const assert = require('assert');
const path = require('path');

const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'nodeAlbum';

// Create a new MongoClient
const client = new MongoClient(url);

let db = null;

client.connect(function (err) {
    // console.log('func connect')
    assert.strictEqual(null, err);
    console.log("Connected successfully to server");

    db = client.db(dbName);
})

function index(req, res) {
    db.collection('images').find({}).toArray(function(err, result) {
        if (err) throw err;
        res.render('image/index', {
            images: result
        });
    });
}

function create(req, res) {
    res.render('image/create');
}

function store(req, res) {
    delete req.body._id;
    db.collection('images').insertOne(req.body)
        .then( result => {
            res.redirect('/images')
        })
        .catch(error => {
            console.log(error);
        });
}

function deleteOne(req, res) {
    console.log(req.params.id);
    const id = req.params.id;
    db.collection('images').findOneAndDelete({"_id": new mongodb.ObjectId(id)}, function(err, result) {
        if (err) throw err;
        console.log("l'élémement avec comme id:" + id + " à bien été supprimé")
        res.render('image/delete', {
            url: result.value.url,
            title: result.value.title,
            desc: result.value.description,
        });
    });
}

function show(req, res) {
    db.collection('images').findOne({"_id": new mongodb.ObjectId(req.params.id)}, function(err, result) {
        if (err) throw err;
        res.render('image/show', result)
    })
}

function edit(req, res) {
    db.collection('images').findOne({"_id": new mongodb.ObjectId(req.params.id)}, function(err, result) {
        if (err) throw err;
        res.render('image/edit', result)
    })
}

function update(req, res) {
    console.log(req.body)
    console.log(req.params)
  db.collection('images').updateOne({"_id": new mongodb.ObjectId(req.params.id)}, {$set: req.body})
    .then(  result => {
        res.redirect('/images')
    })
    .catch( error => {
        if (err) throw err;
    });
}

module.exports = {
    index,
    create,
    store,
    deleteOne,
    show,
    edit,
    update
}